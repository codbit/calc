Calc
====

A very simple calculator project written in C.

A calculator to be used over the terminal and a long-term project, supposed to grow slowly over time.
The point of this project is to offer a fully featured desktop calculator with custom algorithms, this idea sets a basis for
any project that just needs the algorithm or that just needs to follow the same logic.
